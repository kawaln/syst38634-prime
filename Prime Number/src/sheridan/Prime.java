package sheridan;

public class Prime {
	
	
	public static boolean isPrime( int number ) {
		for ( int i = 2 ; i <= number /2 ; i ++) {
			if ( number % i == 0  ) {
				return false;
			}
		}
		return true;
 	}

	public static void main(String[] args) {
		

		System.out.println( "Number 29 is prime? " + Prime.isPrime( 29 ) );
		System.out.println( "Number 29 is prime? " + Prime.isPrime( 30 ) );		
		System.out.println( "Number 33 is prime? " + Prime.isPrime( 33 ) );
		System.out.println( "Number 34 is prime? " + Prime.isPrime( 34 ) );	
		System.out.println( "Number 35 is prime? " + Prime.isPrime( 35 ) );		
		System.out.println( "Number 911 is prime? " + Prime.isPrime( 911 ) );//Edited by Nathaniel Kawal		
		System.out.println( "Number 69 is prime? " + Prime.isPrime( 69 ) );	 //Edited by Nathaniel Kawal
		System.out.println( "Number 50 is prime? " + Prime.isPrime( 50 ) );  //Edited by Nathaniel Kawal
		System.out.println( "Number 89 is prime? " + Prime.isPrime( 89 ) );  //Changed by Dilraz Singh Saini
		System.out.println( "Number 420 is prime? " + Prime.isPrime( 420 ) );  //Added by Dilraz Singh Saini
		
	}

}
